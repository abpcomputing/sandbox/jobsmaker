from setuptools import setup, find_packages, Extension

#######################################
# Prepare list of compiled extensions #
#######################################

# Nothing to be done

#########
# Setup #
#########

setup(
    name='jobsmaker',
    version='0.0.1',
    description='Example python package with compiled extensions',
    url='https://gitlab.cern.ch/abpcomputing/sandbox/jobsmaker',
    author='ABP Computing',
    packages=find_packages(),
    ext_modules = extensions,
    install_requires=[
        'numpy>=1.0',
        'pytest', # In principle could be made optional
        ]
    )
