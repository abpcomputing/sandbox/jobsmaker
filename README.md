# jobsmaker

## Introduction
A beam phycisist (BP) should be as close as possible to the physics of the beam.

Parametric scans of numerical models flourished thanks to the availability 
of unprecedented computing power and the lack, the limit or complexity of the analytical tools. 

The focus of the BP should be on the physics and not on the technicalities of the numerical simulations.
This package aims to contribute in relieve the BP from this unnecessary burden.

The "job" is the atomic unit of the simulation step and it contains all the relevant physics. 
As such, **the BP should harness it**.

A "job" ultimately is a code running on a standalone pc.

The BP can launch several "children" jobs almost identical to the "parent" job with the exception of one or more "mutations".
This set of children jobs can be referred as a "generation".

E.g., from a given and initial "pymask" job with tune (.31,.32) we want to generate a tune scan grid of 10x10 jobs with differen tunes (a tune scan). 
This is a "generation".

Each jobs can be "parent" of other jobs thus propagating a second generation.

The jobs can be run in parallel, locally or remotely.

There are different phases:
- define the parent job.
- apply a cloning
- apply one or more mutations
- apply another generic operation (e.g., the execution or the postprocessing).

The genealogy information is a "map" (e.g., a dictionary, a yaml file, a pandas dataframe) we use to define to describe the parent folder, children folders and mutations.

This package is intended to create the corresponding folders of the "generations" of the BP's study. The goal is to ease the BP's activity **but** maintain him in close contact with the "parent" job (see above, **the BP should harness it**) and offer an adequate flexibility.

## To be done

We try to conform to the style convention of https://www.python.org/dev/peps/pep-0008/
And follow:
1. http://abpcomputing.web.cern.ch/guides/softguide/
2. https://wikis.cern.ch/display/ACCPY/Best+practices+and+style+guidelines

I noted here some sentences for memo.

We suggest to use `yaml` files for the configurations: the `None` object of `python` is translated as `null` in `yaml`.

The json files (at least the pandas.DataFrame "to_json" method does not preserve )

You can use the lxplus python distribution. Login in lxplus and then:
```
source /afs/cern.ch/eng/tracking-tools/python_installations/activate_default_python
```
